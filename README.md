# Dagster Huawei Cloud Resource Components

## Welcome!
Dagster Huawei Cloud Resource Components help you combine Huawei Cloud Services and Dagster capabilities more efficiently!
## Getting Started

```bash
pip install dagster-huaweicloud
```

## Contributing

Thank you for wanting to contribute, you can contribute to [this code repository](https://gitee.com/HuaweiCloudDeveloper/huaweicloud-dagster-components-python) .If you have any questions, consult the community.